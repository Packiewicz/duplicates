// Compiled with: gcc -std=c11 -Wall -pthread -lm

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#define SAMPLE_SIZE 10
#define NEEDLE (SAMPLE_SIZE / 2)

typedef struct
{
    int counter;
    int *array;
    int array_size;
} Generator;

int get_next(Generator *g)
{
    if (g->counter < g->array_size)
        return g->array[g->counter++];
    else
        return -1;
}

// swap all values from array with randomly choosen element
void shuffle(int *arr, int size)
{
    int i, rand_index, temp;
    for(i = 0; i < size; i++)
    {
        rand_index = rand() % size;
        temp = arr[i];
        arr[i] = arr[rand_index];
        arr[rand_index] = temp;
    }
}

void init(Generator *g)
{
    // static int sample_input[SAMPLE_SIZE] =
    //     {0xf12, 0xea2, 0x684, 0xfff, 0x1f5,
    //      0xf12, 0xea2, 0x684, 0xfff, 0x1f5, 0x217};

    g->array = (int *)malloc(SAMPLE_SIZE * sizeof(int));

    int i;
    int half_size = SAMPLE_SIZE / 2;
    for (i = 0; i < half_size; i++)
    {
        if (i != NEEDLE)
        {
            g->array[i] = i;
            g->array[half_size + i] = i;
        }
        else
        {
            g->array[i] = i;
        }
    }
    shuffle(g->array, g->array_size);

    g->array_size = SAMPLE_SIZE;
    g->counter = 0;
}

void reset(Generator *g)
{
    g->counter = 0;
}

void deinit(Generator *g)
{
    free(g->array);
    g->array_size = 0;
    g->counter = 0;
}

int find_in_stack(int *stack, int stack_size, int needle)
{
    int i = 0;
    for (i = 0; i < stack_size; i++)
    {
        if (stack[i] == needle)
            return i;
    }
    return -1;
}

int find_unique_improved(Generator *g)
{
    int stack_size = 0;
    int stack[SAMPLE_SIZE] = {0};
    int i, a, next;

    for (i = 0; i < g->array_size; i++)
    {
        next = get_next(g);
        a = find_in_stack(stack, stack_size, next);
        if (a != -1)
        {
            // replace found duplicate value with last value from stack
            // effectively erases duplicate from stack
            stack[a] = stack[stack_size];
            stack_size--;
        }
        else
        {
            stack[stack_size++] = next;
        }
    }

    if (stack_size > 0)
    {
        return stack[0];
    }
    else
    {
        return -1;
    }
}

int find_unique_xor(Generator *g)
{
    int unique = 0;
    int i, next;
    for (i = 0; i < g->array_size; i++)
    {
        next = get_next(g);
        unique ^= next;
    }
    return unique;
}

#define INT_BITS_SIZE (sizeof(int) * 8)

int find_unique_and(Generator *g)
{
    int array[INT_BITS_SIZE] = {0};
    int i, j, next;
    for(i = 0; i < g->array_size; i++)
    {
        next = get_next(g);
        for(j = 0; j < INT_BITS_SIZE; j++)
        {
            if(next & (1 << j))
            {
                array[j] += 1;
            }
        }
    }

    int result = 0;
    for(j = 0; j < INT_BITS_SIZE; j++)
    {
        if(array[j] == 1)
        {
            result |= (1 << j);
        }
    }
    return result;
}

int compare(const void *a, const void *b)
{
    return ( *(int *)a - *(int *)b);
}

int find_unique_sort(Generator *g)
{
    int *array = (int *)malloc(g->array_size * sizeof(int));
    int i;

    for(i = 0; i < g->array_size; i++)
    {
        array[i] = get_next(g);
    }

    qsort(array, g->array_size, sizeof(int), compare);

    for(i = 0; i < g->array_size; i += 2)
    {
        if(array[i] != array[i + 1])
        {
            return array[i];
        }
    }

    free(array);
    return -1;
}

typedef int (*find_unique_f)(Generator *);
static Generator g;

void measure(const char *name, find_unique_f f)
{
    reset(&g);

    clock_t start = clock();
    int result = f(&g);
    clock_t end = clock();

    if (result != -1)
    {
        clock_t time_diff = end - start;
        float time_seconds = (float)time_diff / CLOCKS_PER_SEC;

        printf("%s\t| %ld \t| %f\n", name, time_diff, time_seconds);
    }
    else
    {
        printf("Error in %s\n", name);
    }
}

int main(void)
{
    init(&g);

    srand(time(NULL));

    printf("Name \t| Ticks \t| Seconds\n");

    measure("XOR", find_unique_xor);
    measure("Improved", find_unique_improved);
    measure("AND", find_unique_and);
    measure("Sort", find_unique_sort);

    deinit(&g);

    return 0;
}
